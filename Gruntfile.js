﻿module.exports = function (grunt) {
    grunt.initConfig({
        // Конкатенирует внешние зависимости, автоматически соблюдая порядок
        bower_concat: {
            js: {
                // Выходной файл таска bower_concat:js
                dest: 'build/_bower.js'
            }
        },
        // Конкатенирует указанные файлы (Соблюдать порядок конкатенации)
        concat: {
            options: {
                separator: ';\n',
            },
            vendor_js: {
                src: [
                    // Результат работы bower_concat:js
                    '<%= bower_concat.js.dest %>',
                    // Внешние зависимости, которые не смог обработать bower_concat
                    'vendor/Chart.min.js',
                    'vendor/angular-chart.js',
                ],
                // Выходной файл таска concat:vendor_js
                dest: 'build/_vendors.js'
            },
            js: {
                src: [
                    // Результат работы concat:vendor_js
                    '<%= concat.vendor_js.dest %>',
                    // Кастом
                    'modules/**/interceptor/*.js',
                    'modules/**/factory/*.js',
                    'modules/**/service/*.js',
                    'modules/**/controller/*.js',
                    'modules/**/app.js',
                    'modules/**/*config.js',
                ],
                // Выходной файл таска concat:js
                dest: 'build/_scripts.js'
            },
            // CSS
            vendor_css: {
                src: [
                    // Внешние зависимости, которые не смог обработать bower_concat
                    'bower_components/bootstrap/dist/css/bootstrap.min.css',
                    'vendor/angular-chart.css',
                ],
                // Выходной файл таска concat:css
                dest: 'build/_vendors.css'
            },
            css: {
                src: [
                    // Результат работы concat:vendor_css
                    '<%= concat.vendor_css.dest %>',
                    // Кастом
                    'css/*.css'
                ],
                // Выходной файл таска concat:css
                dest: 'build/styles.css'
            }
        },
        // Генерация инжекциий зависимостей на основе аннотаций @ngInject и названия аргументов
        ngAnnotate: {
            app: {
                files: {
                    'build/scripts.js': ['<%= concat.js.dest %>']
                },
            },
        },
        // Минификация
        uglify: {
            main: {
                files: {
                    // Выходной файл : Входной файл
                    'build/scripts.js': 'build/scripts.js'
                }
            }
        },
        watch: {
            scripts: {
                files: '<%= concat.js.src %>',
                tasks: ['concat:js', 'ngAnnotate']
            },
            css: {
                files: '<%= concat.css.src %>',
                tasks: 'concat:css'
            },
        }
    });

    // Загрузка плагинов, установленных с помощью npm install
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Задача по умолчанию
    grunt.registerTask('default', ['bower_concat', 'concat', 'ngAnnotate', 'uglify']);
    grunt.registerTask('debug', ['bower_concat', 'concat', 'ngAnnotate']);
    // Debug fast - без обработки вендоров
    grunt.registerTask('df', ['concat:js', 'ngAnnotate']);
    // Debug style
    grunt.registerTask('ds', ['concat:css']);
};