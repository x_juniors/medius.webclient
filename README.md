* Установить node.js (в комплекте идет и NPM)
* Выполнить npm install
* Установить глобально npm install -g bower
* Выполнить bower install
* Установить глобально npm install -g grunt-cli
* Выполнить grunt debug (не минифицировать)
* Или grunt (с минификацией)
* Запустить grunt watch для наблюдения за изменениями