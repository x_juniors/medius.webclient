﻿/**
 * @ngInject
 */
function RightKidneyController($scope, $stateParams, RightKidney) {
    if ($stateParams.usd_id) {
        RightKidney.get({usd_id: $stateParams.usd_id}, function (data) {
            if (data.message !== 'Page not found.') {
                $scope.kidney = data;
            }
        });
    }

    $scope.save = function (data) {
        RightKidney.update({usd_id: $stateParams.usd_id}, data, function (response) {
            if (response.message === 'Page not found.') {
                data.usd = $stateParams.usd_id;
                $scope.kidney = RightKidney.save({usd_id: $stateParams.usd_id}, data);
            }
        });
    };
}