﻿/**
 * @ngInject
 */
function LiverMainController($scope, $stateParams, LiverMain) {
    if ($stateParams.usd_id) {
        LiverMain.get({ usd_id: $stateParams.usd_id }, function (data) {
            if (data.message !== 'Page not found.') {
                $scope.liverMain = data;
            }
        });
    }

    $scope.save = function (data) {
        LiverMain.update({usd_id: $stateParams.usd_id}, data, function (response) {
            if (response.message === 'Page not found.') {
                data.usd = $stateParams.usd_id;
                $scope.liverMain = LiverMain.save({ usd_id: $stateParams.usd_id }, data);
            }
        });
    };
}