﻿/**
 * @ngInject
 */
function AdrenalsController($scope, $stateParams, Adrenals) {
    if ($stateParams.usd_id) {
        Adrenals.get({usd_id: $stateParams.usd_id}, function (data) {
            if (data.message !== 'Page not found.') {
                $scope.adrenals = data;
            }
        });
    }

    $scope.save = function (data) {
        Adrenals.update({usd_id: $stateParams.usd_id}, data, function (response) {
            if (response.message === 'Page not found.') {
                data.usd = $stateParams.usd_id;
                $scope.adrenals = Adrenals.save({usd_id: $stateParams.usd_id}, data);
            }
        });
    };
}