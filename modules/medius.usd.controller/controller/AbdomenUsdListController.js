﻿/**
 * @ngInject
 */
function AbdomenUsdListController($scope, $stateParams, AbdomenUsd) {
    if ($stateParams.patient_id) {
        $scope.usds = AbdomenUsd.queryBy({patient_id: $stateParams.patient_id});
    }
    else {
        $scope.usds = AbdomenUsd.query();
    }
}