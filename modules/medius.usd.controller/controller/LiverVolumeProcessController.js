﻿/**
 * @ngInject
 */
function LiverVolumeProcessController($scope, $stateParams, LiverVolumeProcess) {
    $scope.liverVolumeProcess = new LiverVolumeProcess();
    if ($stateParams.usd_id) {
        LiverVolumeProcess.get({ usd_id: $stateParams.usd_id }, function (data) {
            
            if (data.message !== 'Page not found.') {
                $scope.liverVolumeProcess = data;
            }
        });
    }

    $scope.save = function (data) {
        console.log(data);
        LiverVolumeProcess.update({ usd_id: $stateParams.usd_id }, data, function (response) {

        });
    };
}