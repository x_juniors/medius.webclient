﻿/**
 * @ngInject
 */
function LeftKidneyController($scope, $stateParams, LeftKidney) {
    if ($stateParams.usd_id) {
        LeftKidney.get({usd_id: $stateParams.usd_id}, function (data) {
            if (data.message !== 'Page not found.') {
                $scope.kidney = data;
            }
        });
    }

    $scope.save = function (data) {
        LeftKidney.update({usd_id: $stateParams.usd_id}, data, function (response) {
            if (response.message === 'Page not found.') {
                data.usd = $stateParams.usd_id;
                $scope.kidney = LeftKidney.save({usd_id: $stateParams.usd_id}, data);
            }
        });
    };
}