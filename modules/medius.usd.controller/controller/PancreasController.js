﻿/**
 * @ngInject
 */
function PancreasController($scope, $stateParams, Pancreas) {
    if ($stateParams.usd_id) {
        Pancreas.get({usd_id: $stateParams.usd_id}, function (data) {
            if (data.message !== 'Page not found.') {
                $scope.pancreas = data;
            }
        });
    }

    $scope.save = function (data) {
        Pancreas.update({usd_id: $stateParams.usd_id}, data, function (response) {
            if (response.message === 'Page not found.') {
                data.usd = $stateParams.usd_id;
                $scope.pancreas = Pancreas.save({usd_id: $stateParams.usd_id}, data);
            }
        });
    };
}