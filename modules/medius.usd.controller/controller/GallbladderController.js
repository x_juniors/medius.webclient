﻿/**
 * @ngInject
 */
function GallbladderController($scope, $stateParams, Gallbladder) {
    if ($stateParams.usd_id) {
        Gallbladder.get({usd_id: $stateParams.usd_id}, function (data) {
            if (data.message !== 'Page not found.') {
                $scope.gallbladder = data;
            }
        });
    }

    $scope.save = function (data) {
        Gallbladder.update({usd_id: $stateParams.usd_id}, data, function (response) {
            if (response.message === 'Page not found.') {
                data.usd = $stateParams.usd_id;
                $scope.gallbladder = Gallbladder.save({usd_id: $stateParams.usd_id}, data);
            }
        });
    };
}