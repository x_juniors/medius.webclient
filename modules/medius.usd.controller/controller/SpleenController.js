﻿/**
 * @ngInject
 */
function SpleenController($scope, $stateParams, Spleen) {
    if ($stateParams.usd_id) {
        Spleen.get({usd_id: $stateParams.usd_id}, function (data) {
            if (data.message !== 'Page not found.') {
                $scope.spleen = data;
            }
        });
    }

    $scope.save = function (data) {
        Spleen.update({usd_id: $stateParams.usd_id}, data, function (response) {
            if (response.message === 'Page not found.') {
                data.usd = $stateParams.usd_id;
                $scope.spleen = Spleen.save({usd_id: $stateParams.usd_id}, data);
            }
        });
    };
}