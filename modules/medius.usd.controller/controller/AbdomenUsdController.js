﻿/**
 * @ngInject
 */
function AbdomenUsdController($scope, $stateParams, $state, AbdomenUsd, ApiService) {
    if ($stateParams.usd_id) {
        $scope.usd = AbdomenUsd.get({usd_id: $stateParams.usd_id});
    }

    $scope.saveUsd = function (usd) {
        usd.$update();
    };

    $scope.addUsd = function (usd) {
        usd.patient = $stateParams.patient_id;
        AbdomenUsd.save({patient_id: $stateParams.patient_id}, usd, function (data) {
            $state.go('app.patient.usd', {usd_id: data.id});
        });
    };
    
    $scope.remove = function(usd){
        usd.$remove({}, function(){
            $state.go('app.patient.usds');
        });
    };
    
    $scope.download = function(id){
        window.location = ApiService.baseUrl + '/abdomenusd/' + id + '/download';
    };
}