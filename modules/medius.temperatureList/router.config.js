angular.module('medius.temperatureList').config(function ($stateProvider) {
    var templateDir = 'modules/medius.temperatureList/template/';
    $stateProvider
            .state('app.medcard.temperature_list', {
                url: "/temperature-list",
                templateUrl: templateDir + "temperature_list.html"
            })
            // Temperature
            .state('app.medcard.temperature_list.temperatures', {
                url: "/temperatures",
                templateUrl: templateDir + "temperatures.html",
                controller: TemperaturesController
            })
            .state('app.medcard.temperature_list.temperatures.new', {
                url: "/new",
                templateUrl: templateDir + "newTemperature.html",
                controller: TemperatureController
            })
            .state('app.medcard.temperature_list.temperatures.temperature', {
                url: "/:temperature_id",
                templateUrl: templateDir + "temperature.html",
                controller: TemperatureController
            })
            // Pulse
            .state('app.medcard.temperature_list.pulses', {
                url: "/pulses",
                templateUrl: templateDir + "pulses.html",
                controller: PulsesController
            })
            .state('app.medcard.temperature_list.pulses.new', {
                url: "/new",
                templateUrl: templateDir + "newPulse.html",
                controller: PulseController
            })
            .state('app.medcard.temperature_list.pulses.pulse', {
                url: "/:pulse_id",
                templateUrl: templateDir + "pulse.html",
                controller: PulseController
            })
            // Pressure
            .state('app.medcard.temperature_list.pressures', {
                url: "/pressures",
                templateUrl: templateDir + "pressures.html",
                controller: PressuresController
            })
            .state('app.medcard.temperature_list.pressures.new', {
                url: "/new",
                templateUrl: templateDir + "newPressure.html",
                controller: PressureController
            })
            .state('app.medcard.temperature_list.pressures.pressure', {
                url: "/:pressure_id",
                templateUrl: templateDir + "pressure.html",
                controller: PressureController
            })
            ;
});