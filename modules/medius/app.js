angular.module('medius', [
    'ui.bootstrap',
    'mm.acl',
    'medius.icd',
    'medius.administration',
    'medius.staff',
    'medius.patient',
    'medius.medcard',
    'medius.temperatureList',
    'medius.assignmentList',
    'medius.usd'
]);