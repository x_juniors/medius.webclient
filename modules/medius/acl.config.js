﻿angular.module('medius').run(['$cookies', 'AclService', function ($cookies, AclService) {
    var config = {};
    config.guest = ['sign_in'];
    config.nurse = [
        'app',
        'app.im',
        'app.im.security',
        'app.im.profile',
        'app.help',
        'app.icd',
        'app.class',
        'app.search',
        'app.block',
        'app.patients',
        'app.newPatient',
        'app.patient',
        'app.patient.main',
        'app.patient.locations',
        'app.patient.contacts',
        'app.patient.job',
        'app.patient.medcards',
        'app.patient.usds',
        'app.medcards',
        'app.patient.newMedcard',
        'app.medcard',
        'app.medcard.hospitalization',
        'app.medcard.treatment',
        'app.medcard.temperature_list',
        'app.medcard.temperature_list.temperatures',
        'app.medcard.temperature_list.temperatures.temperature',
        'app.medcard.temperature_list.temperatures.new',
        'app.medcard.temperature_list.pulses',
        'app.medcard.temperature_list.pulses.pulse',
        'app.medcard.temperature_list.pulses.new',
        'app.medcard.temperature_list.pressures',
        'app.medcard.temperature_list.pressures.pressure',
        'app.medcard.temperature_list.pressures.new',
        'app.patient.usd',
        'app.patient.newUsd',
        'app.patient.usd.gallbladder',
        'app.patient.usd.pancreas',
        'app.patient.usd.kidneys',
        'app.patient.usd.kidneys.left',
        'app.patient.usd.kidneys.right',
        'app.patient.usd.spleen',
        'app.patient.usd.adrenals',
        'app.patient.usd.liver',
        'app.patient.usd.liver.liverMain',
        'app.patient.usd.liver.liverVolumeProcess'
    ];
    config.doctor = config.nurse.concat([
        'app.medcard.treatment.operations',
        'app.medcard.treatment.operations.new',
        'app.medcard.treatment.admission_department',
        'app.medcard.treatment.notes',
        'app.medcard.treatment.notes.note',
        'app.medcard.treatment.notes.new',
        'app.medcard.treatment.results',
        'app.medcard.treatment.examination_results',
        'app.medcard.treatment.examination_results.examination_result',
        'app.medcard.treatment.examination_results.new',
        'app.medcard.treatment.expert_inspections',
        'app.medcard.treatment.expert_inspections.expert_inspection',
        'app.medcard.treatment.expert_inspections.expert_inspections_new',
        'app.medcard.treatment.efficiency_marks',
        'app.medcard.treatment.efficiency_marks.efficiency_mark',
        'app.medcard.treatment.efficiency_marks.efficiency_mark_new',
        'app.medcard.treatment.epicrisis',
        'app.medcard.assignment_list',
        'app.medcard.assignment_list.assignments',
        'app.medcard.assignment_list.examinations',
        'app.medcard.assignment_list.diet',
        'app.medcard.assignment_list.physio',
        'app.medcard.assignment_list.psyho',
        'app.medcard.assignment_list.local_treatment',
        'app.medcard.treatment.operation'
    ]);
    config.administrator = config.doctor.concat([
        'app.staff',
        'app.employee',
        'app.employee.account',
        'app.new',
        'app.administration',
        'app.administration.categories',
        'app.administration.categories.view',
        'app.administration.categories.new',
        'app.administration.separations',
        'app.administration.separations.view',
        'app.administration.separations.new',
        'app.administration.contracts.contract',
        'app.administration.posts',
        'app.administration.contracts',
        'app.administration.contracts.new',
        'app.administration.foods',
        'app.administration.foods.new',
        'app.administration.foods.food',
        'app.administration.examinations'
    ]);
    AclService.setAbilities(config);
    var userRole = $cookies.get('role');
    if (userRole == null) {
        AclService.attachRole('guest');
    }
    else {
        AclService.attachRole($cookies.get('role'));
    }
}]);

angular.module('medius').run(['$rootScope', '$state', 'AclService', function ($rootScope, $state, AclService) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (!AclService.can(toState.name)) {
            event.defaultPrevented = true;
            if (AclService.hasRole('guest')) {
                $state.go('sign_in');
            }
            else {
                $state.go('app');
            }
        }
    }
    );
}]);