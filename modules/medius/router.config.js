angular.module('medius').config(function ($stateProvider, $urlRouterProvider) {
    var templateDir = 'modules/medius/template/';
    $urlRouterProvider.otherwise("/app");

    $stateProvider
            .state('app', {
                url: "/app",
                templateUrl: templateDir + "main.html",
                controller: AccountController
            })
            .state('sign_in', {
                url: "/app/sign-in",
                templateUrl: templateDir + "sign_in.html",
                controller: AccountController
            })
            /* help */
            .state('app.help', {
                url: "/help",
                templateUrl: templateDir + "help/help.html"
            })
            /* im */
            .state('app.im', {
                url: "/im",
                templateUrl: templateDir + "im.html",
                controller: ImController
            })
            .state('app.im.security', {
                url: "/security",
                templateUrl: templateDir + "security.html",
                controller: SecurityController
            })
            .state('app.im.profile', {
                url: "/profile",
                templateUrl: templateDir + "profile.html"
            })
            ;
});