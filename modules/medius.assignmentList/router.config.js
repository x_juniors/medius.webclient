angular.module('medius.assignmentList').config(function ($stateProvider) {
    var templateDir = 'modules/medius.assignmentList/template/';
    $stateProvider
            .state('app.medcard.assignment_list', {
                url: "/assignment-list",
                templateUrl: templateDir + "assignment_list.html"
            })
            // medcard.assignment_list.*
            .state('app.medcard.assignment_list.assignments', {
                url: "/assignments",
                templateUrl: templateDir + "assignments.html"
            })
            .state('app.medcard.assignment_list.examinations', {
                url: "/examinations",
                templateUrl: templateDir + "examinations.html"
            })
            .state('app.medcard.assignment_list.diet', {
                url: "/diet",
                templateUrl: templateDir + "diet.html"
            })
            .state('app.medcard.assignment_list.physio', {
                url: "/physio",
                templateUrl: templateDir + "physio.html"
            })
            .state('app.medcard.assignment_list.psyho', {
                url: "/psyho",
                templateUrl: templateDir + "psyho.html"
            })
            .state('app.medcard.assignment_list.local_treatment', {
                url: "/local_treatment",
                templateUrl: templateDir + "local_treatment.html"
            })
            ;
});