angular.module('medius.medcard').config(function ($stateProvider) {
    var templateDir = 'modules/medius.medcard/template/';
    $stateProvider
            // medcard
            .state('app.medcards', {
                url: "/medcards",
                templateUrl: templateDir + "medcards.html",
                controller: MedcardListController
            })
            .state('app.patient.newMedcard', {
                url: "/medcards/new",
                templateUrl: templateDir + "new.html",
                controller: MedcardNewController
            })
            .state('app.medcard', {
                url: "/medcards/:medcard_id",
                templateUrl: templateDir + "medcard.html",
                controller: MedcardController
            })
            .state('app.medcard.hospitalization', {
                url: "/hospitalization",
                templateUrl: templateDir + "hospitalization.html",
                controller: HospitalizationController
            })
            .state('app.medcard.treatment', {
                url: "/treatment",
                templateUrl: templateDir + "treatment.html",
                controller: TreatmentController
            })
            // medcard.treatment.*
            .state('app.medcard.treatment.operations', {
                url: "/operations",
                templateUrl: templateDir + "treatment/operations.html",
                controller: OperationController
            })
            .state('app.medcard.treatment.operations.new', {
                url: "/new",
                templateUrl: templateDir + "treatment/operation_new.html",
                controller: NewOperationController
            })

            .state('app.medcard.treatment.operation', {
                url: "/operations/:operation_id",
                templateUrl: templateDir + "treatment/operation.html",
                controller: OperationController
            })
            .state('app.medcard.treatment.admission_department', {
                url: "/admission-department",
                templateUrl: templateDir + "treatment/admission_department.html",
                controller: AdmissionDepartmentController
            })
            .state('app.medcard.treatment.notes', {
                url: "/notes",
                templateUrl: templateDir + "treatment/notes.html",
                controller: NotebooksController
            })
            .state('app.medcard.treatment.notes.note', {
                url: "/:notebook_id",
                templateUrl: templateDir + "treatment/note.html",
                controller: NotebookController
            })
            .state('app.medcard.treatment.notes.new', {
                url: "/new",
                templateUrl: templateDir + "treatment/note_new.html",
                controller: NewNotebookController
            })
            .state('app.medcard.treatment.examination_results', {
                url: "/examination-results",
                templateUrl: templateDir + "treatment/examination_results.html",
                controller: ExaminationResultsController
            })

            .state('app.medcard.treatment.examination_results.examination_result', {
                url: "/:examinationresult_id",
                templateUrl: templateDir + "treatment/examination_result.html",
                controller: ExaminationResultController
            })
            .state('app.medcard.treatment.examination_results.new', {
                url: "/new",
                templateUrl: templateDir + "treatment/examination_result_new.html",
                controller: NewExaminationResultController
            })
            .state('app.medcard.treatment.expert_inspections', {
                url: "/expert-inspections",
                templateUrl: templateDir + "treatment/expert_inspections.html",
                controller: ExpertInspectionsController
            })
            .state('app.medcard.treatment.expert_inspections.expert_inspections_new', {
                url: "/new",
                templateUrl: templateDir + "treatment/expert_inspectionsNew.html",
                controller: ExpertInspectionNewController
            })
            .state('app.medcard.treatment.expert_inspections.expert_inspection', {
                url: "/:expertinspection_id",
                templateUrl: templateDir + "treatment/expert_inspection.html",
                controller: ExpertInspectionController
            })
            .state('app.medcard.treatment.efficiency_marks', {
                url: "/efficiency-marks",
                templateUrl: templateDir + "treatment/efficiency_marks.html",
                controller: EfficiencyMarksController
            })
            .state('app.medcard.treatment.efficiency_marks.efficiency_mark_new', {
                url: "/new",
                templateUrl: templateDir + "treatment/efficiency_markNew.html",
                controller: EfficiencyMarkNewController
            })
            .state('app.medcard.treatment.efficiency_marks.efficiency_mark', {
                url: "/:efficiencymark_id",
                templateUrl: templateDir + "treatment/efficiency_mark.html",
                controller: EfficiencyMarkController
            })
            .state('app.medcard.treatment.epicrisis', {
                url: "/epicrisis",
                templateUrl: templateDir + "treatment/epicrisis.html"
            })
            ;
});