/**
 * @ngInject
 */
function IcdClassController($scope, IcdClass) {
    $scope.classes = IcdClass.query();
}