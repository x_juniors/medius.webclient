angular.module('medius.administration').config(function ($stateProvider) {
    var templateDir = 'modules/medius.administration/template/';
    $stateProvider
            .state('app.administration', {
                url: "/administration",
                templateUrl: templateDir + "administration.html"
            })
            .state('app.administration.categories', {
                url: "/categories",
                templateUrl: templateDir + "categories.html",
                controller: CategoriesController
            })
            .state('app.administration.categories.view', {
                url: "/:category_id",
                templateUrl: templateDir + "category.html",
                controller: CategoryController
            })
            .state('app.administration.categories.new', {
                url: "/new",
                templateUrl: templateDir + "category_new.html",
                controller: NewCategoryController
            })
            .state('app.administration.separations', {
                url: "/separations",
                templateUrl: templateDir + "separations.html",
                controller: SeparationsController
            })
            .state('app.administration.separations.view', {
                url: "/:separation_id",
                templateUrl: templateDir + "separation.html",
                controller: SeparationController
            })
            .state('app.administration.separations.new', {
                url: "/new",
                templateUrl: templateDir + "separation_new.html",
                controller: NewSeparationController
            })
            .state('app.administration.posts', {
                url: "/posts",
                templateUrl: templateDir + "posts.html",
                controller: PostsController
            })
            .state('app.administration.contracts', {
                url: "/contracts",
                templateUrl: templateDir + "contracts.html",
                controller: ContractsController
            })
            .state('app.administration.contracts.new', {
                url: "/new",
                templateUrl: templateDir + "contract_new.html",
                controller: NewContractController
            })
            .state('app.administration.contracts.contract', {
                url: "/:contract_id",
                templateUrl: templateDir + "contract.html",
                controller: ContractController
            })
            .state('app.administration.foods', {
                url: "/foods",
                templateUrl: templateDir + "foods.html",
                controller: FoodsController
            })
            .state('app.administration.foods.new', {
                url: "/new",
                templateUrl: templateDir + "food_new.html",
                controller: NewFoodController
            })
            .state('app.administration.foods.food', {
                url: "/:food_id",
                templateUrl: templateDir + "food.html",
                controller: FoodController
            })
            .state('app.administration.examinations', {
                url: "/examinations",
                templateUrl: templateDir + "examinations.html",
                controller: ExaminationTypesController
            })
            ;
});