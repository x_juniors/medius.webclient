﻿/**
 * @ngInject
 */
function ExaminationResults($resource, ExaminationResultGetInterceptor, ExaminationResultQueryInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/examinationresults/:examinationresult_id', {examinationresult_id: "@id"}, {
        queryBy: {
            method: 'GET', url: ApiService.baseUrl + '/medcards/:medcard_id/examinationresults', isArray: true,
            interceptor: ExaminationResultQueryInterceptor
        },
        getBy: {
            method: 'GET',
            interceptor: ExaminationResultGetInterceptor
        },
        save: {
            method: 'POST', url: ApiService.baseUrl + '/medcards/:medcard_id/examinationresults',
            interceptor: ExaminationResultGetInterceptor
        },
        update: { method: 'PUT' }
    });
}