/**
 * @ngInject
 */
function Pressure($resource, DateQueryInterceptor, DateGetInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/pressures/:pressure_id',
        { pulse_id: "@date" },
        {
            queryBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/medcards/:medcard_id/pressures',
                isArray: true,
                interceptor: DateQueryInterceptor
            },
            get: {
                method: 'GET',
                interceptor: DateGetInterceptor
            },
            update: {
                method: 'PUT'
            },
            save: {
                method: 'POST',
                url: ApiService.baseUrl + '/medcards/:medcard_id/pressures',
                interceptor: DateGetInterceptor
            }
        });
}