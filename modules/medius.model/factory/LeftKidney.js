/**
 * @ngInject
 */
function LeftKidney($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/leftkidney',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}