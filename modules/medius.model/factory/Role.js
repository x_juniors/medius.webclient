﻿/**
 * @ngInject
 */
function Role($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/roles/:role_id', { role_id: "@id" }, {
        queryBy: { method: 'GET', isArray: true }
    });
}