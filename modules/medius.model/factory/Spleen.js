/**
 * @ngInject
 */
function Spleen($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/spleen',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}