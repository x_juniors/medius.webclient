﻿/**
 * @ngInject
 */
function Separations($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/separations/:separation_id', { separation_id: "@id" }, {
        queryBy: { method: 'GET', isArray: true },
        getBy: { method: 'GET' },
        save: { method: 'POST' },
        update: { method: 'PUT' }
    });
}