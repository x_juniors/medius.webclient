﻿/**
 * @ngInject
 */
function Region($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/regions/:region_id', {}, {
        queryFilter: { method: 'GET', url: ApiService.baseUrl + '/regions?expression=:expression&limit=:limit', params: { limit: 10 }, isArray: true },
        getBy: { method: 'GET', url: ApiService.baseUrl + '/patients/:patient_id/location/street/locality/region' }
    });
}