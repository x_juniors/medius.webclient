﻿/**
 * @ngInject
 */
function Categories($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/categories/:category_id', { category_id: "@id" }, {
        queryBy: { method: 'GET', isArray: true },
        getBy: { method: 'GET' },
        save: { method: 'POST' },
        update: { method: 'PUT' }
    });
}