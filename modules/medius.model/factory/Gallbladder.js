/**
 * @ngInject
 */
function Gallbladder($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/gallbladder',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}