﻿/**
 * @ngInject
 */
function Posts($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/posts/:post_id', { post_id: "@id" }, {
        queryBy: { method: 'GET', isArray: true }
    });
}