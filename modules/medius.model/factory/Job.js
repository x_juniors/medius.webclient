﻿/**
 * @ngInject
 */
function Job($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/jobs/:job_id', { job_id: '@id' }, {
        getBy: { method: 'GET', url: ApiService.baseUrl + '/patients/:patient_id/job' },
        queryBy: { method: 'GET', url: ApiService.baseUrl + '/organizations/:organization_id/jobs', params: { organization_id: '@organizationId' }, isArray: true },
        queryFilter: { method: 'GET', url: ApiService.baseUrl + '/organizations/:organization_id/jobs?expression=:expression&limit=:limit', params: { limit: 10 }, isArray: true },
        save: { method: 'POST', url: 'api/api/organizations/:organization_id/jobs', params: { organization_id: "@organizationId" } }
    });
}