/**
 * @ngInject
 */
function Operation($resource, OperationQueryInterceptor, OperationGetInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/operations/:operation_id', { operation_id: "@id" }, {
        queryBy: {
            method: 'GET', url: ApiService.baseUrl + '/medcards/:medcard_id/operations', isArray: true,
            interceptor: OperationQueryInterceptor
        },
        get: {
            method:'GET', 
            interceptor: OperationGetInterceptor
        },
        save: {
            method: 'POST', url: ApiService.baseUrl + '/medcards/:medcard_id/operations',
            interceptor: OperationGetInterceptor
        },
        update: { method: 'PUT' },
        remove: { method: 'DELETE'}
    });
}