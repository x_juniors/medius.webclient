﻿/**
 * @ngInject
 */
function ExaminationTypes($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/examinationtypes', { }, {
        queryBy: { method: 'GET', url: ApiService.baseUrl + '/examinationtypes', isArray: true }
    });
}