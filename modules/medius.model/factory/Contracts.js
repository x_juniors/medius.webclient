﻿/**
 * @ngInject
 */
function Contracts($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/contracts/:contract_id', { contract_id: "@id" }, {
        queryBy: { method: 'GET', isArray: true },
        getBy: { method: 'GET' },
        update: { method: 'PUT' }
    });
}