﻿/**
 * @ngInject
 */
function Notebooks($resource, NotebookGetInterceptor, NotebookQueryInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/notebooks/:notebook_id', {notebook_id: "@id"}, {
        queryBy: {
            method: 'GET', url: ApiService.baseUrl + '/medcards/:medcard_id/notebooks', isArray: true,
            interceptor: NotebookQueryInterceptor
        },
        getBy: {
            method: 'GET',
            interceptor: NotebookGetInterceptor
        },
        save: {
            method: 'POST', url: ApiService.baseUrl + '/medcards/:medcard_id/notebooks',
            interceptor: NotebookGetInterceptor
        },
        update: {
            method: 'PUT'
        }
    });
}