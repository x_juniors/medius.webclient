/**
 * @ngInject
 */
function Hospitalization($resource, HospitalizationInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/hospitalizations/:hospitalization_id',
        { hospitalization_id: "@medcardId" },
        {
            getBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/medcards/:medcard_id/hospitalization',
                interceptor: HospitalizationInterceptor
            },
            update: {
                method: 'PUT'
            }
        });
}