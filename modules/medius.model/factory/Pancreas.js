/**
 * @ngInject
 */
function Pancreas($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/pancreas',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}