﻿/**
 * @ngInject
 */
function TreatmentDiagnose($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/treatmentdiagnoses/:treatment_id', { treatment_id: "@medcardId" }, {
        queryBy: { method: 'GET', url: ApiService.baseUrl + '/medcards/:medcard_id/treatmentdiagnose' },
        update: { method: 'PUT' }
    });
}