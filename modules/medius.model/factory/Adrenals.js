/**
 * @ngInject
 */
function Adrenals($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/adrenals',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}