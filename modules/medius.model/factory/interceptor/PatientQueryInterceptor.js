﻿/**
 * @ngInject
 */
function PatientQueryInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            for (var i = 0; i < response.resource.length; i++) {
                response.resource[i].born = MediusDateFormatService.getDate(response.resource[i].born);
            }
            return response.resource;
        }
    };
}