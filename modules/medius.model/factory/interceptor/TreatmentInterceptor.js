﻿/**
 * @ngInject
 */
function TreatmentInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            response.resource.oncologicalObserving = MediusDateFormatService.getDate(response.resource.oncologicalObserving);
            response.resource.xrayObserving = MediusDateFormatService.getDate(response.resource.xrayObserving);
            return response.resource;
        }
    };
}