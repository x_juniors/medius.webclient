﻿/**
 * @ngInject
 */
function PatientInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            response.resource.born = MediusDateFormatService.getDate(response.resource.born);
            return response.resource;
        }
    };
}