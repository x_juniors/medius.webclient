﻿/**
 * @ngInject
 */
function HospitalizationInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            response.resource.settingDate = MediusDateFormatService.getDate(response.resource.settingDate);
            return response.resource;
        }
    };
}