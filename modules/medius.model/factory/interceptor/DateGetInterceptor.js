﻿/**
 * @ngInject
 */
function DateGetInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            response.resource.date = MediusDateFormatService.getDate(response.resource.date);
            return response.resource;
        }
    };
}