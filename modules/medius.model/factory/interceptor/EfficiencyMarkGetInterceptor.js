﻿/**
 * @ngInject
 */
function EfficiencyMarkGetInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            response.resource.begin = MediusDateFormatService.getDate(response.resource.begin);
            response.resource.end = MediusDateFormatService.getDate(response.resource.end);
            return response.resource;
        }
    };
}