﻿/**
 * @ngInject
 */
function MedcardInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            response.resource.hospitalizationDate = MediusDateFormatService.getDate(response.resource.hospitalizationDate);
            response.resource.hepatitisDate = MediusDateFormatService.getDate(response.resource.hepatitisDate);
            response.resource.hivInfection = MediusDateFormatService.getDate(response.resource.hivInfection);
            response.resource.rw = MediusDateFormatService.getDate(response.resource.rw);
            return response.resource;
        }
    };
}