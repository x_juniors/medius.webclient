﻿/**
 * @ngInject
 */
function TokenQueryInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            for (var i = 0; i < response.resource.length; i++) {
                response.resource[i].expiresIn = MediusDateFormatService.getDate(response.resource[i].expiresIn);
            }
            return response.resource;
        }
    };
}