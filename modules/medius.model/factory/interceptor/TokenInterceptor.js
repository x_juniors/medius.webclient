﻿/**
 * @ngInject
 */
function TokenInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            if (response.resource.token) {
                response.resource.token.expiresIn = MediusDateFormatService.getDate(response.resource.token.expiresIn);
            }
            return response.resource;
        }
    };
}