﻿/**
 * @ngInject
 */
function EfficiencyMarkQueryInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            for (var i = 0; i < response.resource.length; i++) {
                response.resource[i].begin = MediusDateFormatService.getDate(response.resource[i].begin);
                response.resource[i].end = MediusDateFormatService.getDate(response.resource[i].end);
            }
            return response.resource;
        }
    };
}