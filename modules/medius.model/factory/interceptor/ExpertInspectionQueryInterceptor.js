﻿/**
 * @ngInject
 */
function ExpertInspectionQueryInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            for (var i = 0; i < response.resource.length; i++) {
                response.resource[i].date = MediusDateFormatService.getDate(response.resource[i].date);
            }
            return response.resource;
        }
    };
}