﻿/**
 * @ngInject
 */
function MedcardQueryInterceptor(MediusDateFormatService) {
    return {
        response: function (response) {
            for (var i = 0; i < response.resource.length; i++) {
                response.resource[i].hospitalizationDate = MediusDateFormatService.getDate(response.resource[i].hospitalizationDate);
            }
            return response.resource;
        }
    };
}