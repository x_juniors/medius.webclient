﻿/**
 * @ngInject
 */
function LiverVolumeProcess($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/liverVolumeProcess',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}