﻿/**
 * @ngInject
 */
function Street($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/streets/:street_id', {}, {
        queryBy: { method: 'GET', url: ApiService.baseUrl + '/localities/:locality_id/streets', isArray: true },
        getBy: { method: 'GET', url: ApiService.baseUrl + '/patients/:patient_id/location/street' },
        queryFilter: { method: 'GET', url: ApiService.baseUrl + '/localities/:locality_id/streets?expression=:expression&limit=:limit', params: { limit: 10 }, isArray: true },
        save: { method: 'POST', url: 'api/api/localities/:locality_id/streets', params: { locality_id: "@localityId" } }
    });
}