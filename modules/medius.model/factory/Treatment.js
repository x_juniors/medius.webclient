/**
 * @ngInject
 */
function Treatment($resource, TreatmentInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/treatments/:treatment_id',
        { treatment_id: "@medcardId" }, {
            queryBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/medcards/:medcard_id/treatment',
                interceptor: TreatmentInterceptor
            },
            update: {
                method: 'PUT'
            }
        });
}