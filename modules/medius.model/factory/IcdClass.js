/**
 * @ngInject
 */
function IcdClass($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/icdclasses/:class_id');
}