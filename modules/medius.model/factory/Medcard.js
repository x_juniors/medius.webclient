/**
 * @ngInject
 */
function Medcard($resource, MedcardInterceptor, MedcardQueryInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/medcards/:medcard_id',
        { medcard_id: "@id" },
        {
            get: {
                method: 'GET',
                interceptor: MedcardInterceptor
            },
            query: {
                method: 'GET',
                isArray: true,
                interceptor: MedcardQueryInterceptor
            },
            queryBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/patients/:patient_id/medcards',
                isArray: true,
                interceptor: MedcardQueryInterceptor
            },
            update: {
                method: 'PUT'
            },
            save: {
                method: 'POST', url: ApiService.baseUrl + '/patients/:patient_id/medcards',
                interceptor: MedcardInterceptor
            },
        });
}