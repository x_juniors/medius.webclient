/**
 * @ngInject
 */
function Patient($resource, ApiService, PatientInterceptor, PatientQueryInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/patients/:patient_id',
            {patient_id: '@id'},
    {
        query: {
            method: 'GET',
            isArray: true,
            interceptor: PatientQueryInterceptor
        },
        get: {
            method: 'GET',
            interceptor: PatientInterceptor
        },
        update: {
            method: 'PUT'
        },
        save: {
            method: 'POST',
            interceptor: PatientInterceptor
        },
        updateJob: {
            method: 'PUT', url: ApiService.baseUrl + '/patients/:patient_id/jobs/:job_id', params: {job_id: "@jobId"}
        }
    });
}