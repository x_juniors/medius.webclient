﻿/**
 * @ngInject
 */
function Account($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/accounts/:account_id', { account_id: "@id" }, {
        update: { method: 'PUT' },
        changePassword: { method: 'PUT', url: ApiService.baseUrl + '/accounts/:account_id/password' },
        changeRole: {method: 'PUT', url: ApiService.baseUrl + '/accounts/:account_id/roles/:role_id'}
    });
}