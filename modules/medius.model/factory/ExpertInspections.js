﻿/**
 * @ngInject
 */
function ExpertInspections($resource, ExpertInspectionQueryInterceptor, ExpertInspectionGetInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/expertinspections/:expertinspections_id', { expertinspections_id: "@id" }, {
        queryBy: {
            method: 'GET', url: ApiService.baseUrl + '/medcards/:medcard_id/expertinspections', isArray: true,
            interceptor: ExpertInspectionQueryInterceptor
        },
        save: {
            method: 'POST', url: ApiService.baseUrl + '/medcards/:medcard_id/expertinspections',
            interceptor: ExpertInspectionGetInterceptor
        },
        getBy: {
            method: 'GET',
            interceptor: ExpertInspectionGetInterceptor
        },
        update: { method: 'PUT' },
        remove: { method: 'DELETE' }
    });
}