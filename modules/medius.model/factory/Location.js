﻿/**
 * @ngInject
 */
function Location($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/patients/:patient_id/location', { }, {
        queryBy: { method: 'GET', isArray: false },
        update: { method: 'PUT' },
        updateStreet: { method: 'PUT', url: ApiService.baseUrl + '/locations/:location_id/streets/:street_id', params: { street_id: "@streetId" } }
    });
}