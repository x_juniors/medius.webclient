﻿/**
 * @ngInject
 */
function EfficiencyMarks($resource, EfficiencyMarkQueryInterceptor, EfficiencyMarkGetInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/efficiencymarks/:efficiencymarks_id', { efficiencymarks_id: "@id" }, {
        queryBy: {
            method: 'GET', url: ApiService.baseUrl + '/medcards/:medcard_id/efficiencymarks', isArray: true,
            interceptor: EfficiencyMarkQueryInterceptor
        },
        save: {
            method: 'POST', url: ApiService.baseUrl + '/medcards/:medcard_id/efficiencymarks',
            interceptor: EfficiencyMarkGetInterceptor
        },
        getBy: {
            method: 'GET',
            interceptor: EfficiencyMarkGetInterceptor
        },
        update: { method: 'PUT' }
    });
}