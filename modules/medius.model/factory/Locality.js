﻿/**
 * @ngInject
 */
function Locality($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/localities/:locality_id', { locality_id: '@id' }, {
        queryBy: { method: 'GET', url: ApiService.baseUrl + '/regions/:region_id/localities', isArray: true },
        getBy: { method: 'GET', url: ApiService.baseUrl + '/patients/:patient_id/location/street/locality' },
        queryFilter: { method: 'GET', url: ApiService.baseUrl + '/regions/:region_id/localities?expression=:expression&limit=:limit', params: { limit: 10 }, isArray: true },
        update: { method: 'PUT' }
    });
}