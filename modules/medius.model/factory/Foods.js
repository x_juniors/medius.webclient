﻿/**
 * @ngInject
 */
function Foods($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/foods/:food_id', { food_id: "@id" }, {
        queryBy: { method: 'GET', url: ApiService.baseUrl + '/foods', isArray: true },
        getBy: { method: 'GET' },
        update: { method: 'PUT' }
    });
}