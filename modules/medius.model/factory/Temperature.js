/**
 * @ngInject
 */
function Temperature($resource, DateQueryInterceptor, DateGetInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/temperatures/:temperature_id',
        { temperature_id: "@date" },
        {
            queryBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/medcards/:medcard_id/temperatures',
                isArray: true,
                interceptor: DateQueryInterceptor
            },
            get: {
                method: 'GET',
                interceptor: DateGetInterceptor
            },
            update: {
                method: 'PUT'
            },
            save: {
                method: 'POST',
                url: ApiService.baseUrl + '/medcards/:medcard_id/temperatures',
                interceptor: DateGetInterceptor
            }
        });
}