﻿/**
 * @ngInject
 */
function Organization($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/organizations/:organization_id', {}, {
        getBy: { method: 'GET', url: ApiService.baseUrl + '/patients/:patient_id/job/organization' },
        queryFilter: { method: 'GET', url: ApiService.baseUrl + '/organizations?expression=:expression&limit=:limit', params: { limit: 10 }, isArray: true }
    });
}