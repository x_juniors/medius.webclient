/**
 * @ngInject
 */
function AbdomenUsd($resource, DateQueryInterceptor, DateGetInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id',
        { usd_id: "@id" },
        {
            queryBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/patients/:patient_id/abdomenusd',
                isArray: true,
                interceptor: DateQueryInterceptor
            },
            get: {
                method: 'GET',
                interceptor: DateGetInterceptor
            },
            update: {
                method: 'PUT'
            },
            save: {
                method: 'POST',
                url: ApiService.baseUrl + '/patients/:patient_id/abdomenusd',
                interceptor: DateGetInterceptor
            }
        });
}