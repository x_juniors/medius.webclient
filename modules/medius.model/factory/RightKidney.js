/**
 * @ngInject
 */
function RightKidney($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/rightkidney',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}