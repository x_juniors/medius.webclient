/**
 * @ngInject
 */
function Pulse($resource, DateQueryInterceptor, DateGetInterceptor, ApiService) {
    return $resource(ApiService.baseUrl + '/pulses/:pulse_id',
        { pulse_id: "@date" },
        {
            queryBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/medcards/:medcard_id/pulses',
                isArray: true,
                interceptor: DateQueryInterceptor
            },
            get: {
                method: 'GET',
                interceptor: DateGetInterceptor
            },
            update: {
                method: 'PUT'
            },
            save: {
                method: 'POST',
                url: ApiService.baseUrl + '/medcards/:medcard_id/pulses',
                interceptor: DateGetInterceptor
            }
        });
}