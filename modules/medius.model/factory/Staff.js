﻿/**
 * @ngInject
 */
function Staff($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/staffs/:staff_id', { staff_id: "@id" }, {
        queryBy: { method: 'GET', isArray: true },
        getBy: { method: 'GET' },
        update: { method: 'PUT' }
    });
}