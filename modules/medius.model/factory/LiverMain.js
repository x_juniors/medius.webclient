/**
 * @ngInject
 */
function LiverMain($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/abdomenusd/:usd_id/liverMain',
            {},
            {
                update: {
                    method: 'PUT'
                }
            });
}