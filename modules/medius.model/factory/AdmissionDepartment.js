﻿/**
 * @ngInject
 */
function AdmissionDepartment($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/admissiondepartments/:admission_id', { admission_id: "@medcardId" }, {
        queryBy: { method: 'GET', url: ApiService.baseUrl + '/medcards/:medcard_id/admissiondepartment', isArray: false },
        update: { method: 'PUT' },
        save: { method: 'POST', url: ApiService.baseUrl + '/medcards/:medcard_id/admissiondepartment' }
    });
}