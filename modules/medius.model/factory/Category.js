﻿/**
 * @ngInject
 */
function Category($resource, ApiService) {
    return $resource(ApiService.baseUrl + '/categories/:category_id', {}, {
        queryFilter: { method: 'GET', url: ApiService.baseUrl + '/categories?expression=:expression&limit=:limit', params: { limit: 10 }, isArray: true },
        getBy: { method: 'GET', url: ApiService.baseUrl + '/patients/:patient_id/category' }
    });
}