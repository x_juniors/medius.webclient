﻿/**
 * @ngInject
 */
function Token($resource, ApiService, TokenInterceptor, TokenQueryInterceptor) {
    return $resource(ApiService.baseUrl + '/tokens/:token_id',
        { token_id: '@value' },
        {
            authenticate: {
                method: 'POST',
                url: ApiService.baseUrl + '/tokens',
                interceptor: TokenInterceptor
            },
            queryBy: {
                method: 'GET',
                url: ApiService.baseUrl + '/accounts/:account_id/tokens',
                isArray: true,
                interceptor: TokenQueryInterceptor
            },
            clearExcept: {
                method: 'DELETE',
                url: ApiService.baseUrl + '/accounts/:account_id/tokens'
            }
        }
    );
}