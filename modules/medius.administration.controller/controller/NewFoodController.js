﻿/**
 * @ngInject
 */
function NewFoodController($scope, $state, $stateParams, Foods) {
    $scope.loadNewFood = function () {
        $scope.newFood = {
            title: null
        }
    }

    $scope.loadNewFood();

    $scope.saveNewFood = function (data) {
        if ($scope.newFood.title != null) {
            var newFood = angular.copy(data);
            Foods.save({ food_id: $stateParams.food_id }, newFood, function () {
                $scope.loadFoods();
                $state.go('app.administration.foods');
            });
        } else alert("Insert all data!!! Please!");
    }

    $scope.cencelNewFood = function () {
        $state.go('app.administration.foods');

    }
}