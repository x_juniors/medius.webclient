﻿/**
 * @ngInject
 */
function FoodsController($scope, $stateParams, Foods) {
    $scope.loadFoods = function() {
        $scope.foods = Foods.queryBy({ isArray: true });
    }

    $scope.loadFoods();

}