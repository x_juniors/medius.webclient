﻿/**
 * @ngInject
 */
function NewSeparationController($scope, $state, $stateParams, Separations) {
    $scope.loadNewSeparation = function () {
        $scope.newSeparation = {
            title: null
        }
    }

    $scope.loadNewSeparation();

    $scope.saveSeparation = function (data) {
        if ($scope.newSeparation.title != null) {
            var separation = angular.copy(data);
            Separations.save({ separation_id: $stateParams.separation_id }, separation, function () {
                $scope.loadSeparations();
                $state.go('app.administration.separations');
            });
        } else alert("Insert all data!!! Please!");
    }

    $scope.cencelSeparation = function () {
        $state.go('app.administration.separations');

    }
}