﻿/**
 * @ngInject
 */
function NewContractController($scope, $state, $stateParams, Contracts) {
    $scope.loadNewContract = function () {
        $scope.newContract = {
            title: null,
            description: null,
            subject: null
        }
    }

    $scope.loadNewContract();

    $scope.saveNewContract = function (data) {
        var contract = angular.copy(data);

        Contracts.save({ contract_id: $stateParams.contract_id }, contract, function () {
            $scope.getContracts();
            $state.go('app.administration.contracts');
        });
    }
    $scope.cencelNewContract = function () {
        $state.go('app.administration.contracts');
    }
}