﻿/**
 * @ngInject
 */
function CategoriesController($scope, $stateParams, Categories) {
    $scope.loadCategories = function() {
        Categories.queryBy({ isArray: true }, function(data) {
            $scope.categories = data;
        });
    }
    $scope.loadCategories();
}