﻿/**
 * @ngInject
 */
function ContractController($scope, $stateParams, Contracts) {
    Contracts.getBy({ contract_id: $stateParams.contract_id }, function (data) {
        $scope.contract = data;
    });

    $scope.updateContract = function (data) {
        var contract = angular.copy(data);
        contract.$update({}, function () {
            $scope.getContracts();
        });
    }
    $scope.removeContract = function(data) {
        data.$remove({}, function() {
            $scope.getContracts();
        });
    }
}