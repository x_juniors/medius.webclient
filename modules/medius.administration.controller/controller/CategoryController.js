﻿/**
 * @ngInject
 */
function CategoryController($scope, $state, $stateParams, Categories) {
    $scope.loadCategory = function () {
        if ($stateParams.category_id) {
            Categories.getBy({ category_id: $stateParams.category_id }, function (data) {
                $scope.category = data;
            });
        }
    }

    $scope.loadCategory();

    $scope.updateCategory = function (data) {
        var category = angular.copy(data);
        category.$update({}, function () {
            $scope.loadCategories();
            $state.go('app.administration.categories');
        });

    }

    $scope.removeCategory = function (data) {
        data.$remove({}, function () {
            $scope.loadCategories();
        });
        $state.go('app.administration.categories');

    }

    $scope.cencelCategory = function () {
        $state.go('app.administration.categories');
    }
}