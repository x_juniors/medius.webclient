﻿/**
 * @ngInject
 */
function NewCategoryController($scope, $state, $stateParams, Categories) {
    $scope.loadNewCategory = function () {
        $scope.newCategory = {
            title: null
        }
    }

    $scope.loadNewCategory();

    $scope.saveCategory = function (data) {
        if ($scope.newCategory.title != null) {
            var category = angular.copy(data);
            Categories.save({ category_id: $stateParams.category_id }, category, function () {
                $scope.loadCategories();
                $state.go('app.administration.categories');
            });
        } else alert("Insert all data!!! Please!");
    }

    $scope.cencelCategory = function () {
        $state.go('app.administration.categories');

    }
}