﻿/**
 * @ngInject
 */
function SeparationController($scope, $state, $stateParams, Separations) {
    $scope.loadSeparation = function () {
        if ($stateParams.separation_id) {
            Separations.getBy({ separation_id: $stateParams.separation_id }, function (data) {
                $scope.separation = data;
            });
        }
    }

    $scope.loadSeparation();

    $scope.updateSeparation = function (data) {
        var separation = angular.copy(data);
        separation.$update({}, function () {
            $scope.loadSeparations();
            $state.go('app.administration.separations');
        });

    }

    $scope.removeSeparation = function (data) {
        data.$remove({}, function () {
            $scope.loadSeparations();
        });
        $state.go('app.administration.separations');

    }

    $scope.cencelSeparation = function () {
        $state.go('app.administration.separations');
    }
}