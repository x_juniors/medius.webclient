﻿/**
 * @ngInject
 */
function FoodController($scope, $state, $stateParams, Foods) {
    $scope.loadFood = function () {
        if ($stateParams.food_id) {
            Foods.getBy({ food_id: $stateParams.food_id }, function (data) {
                $scope.food = data;
            });
        }
    }

    $scope.loadFood();

    $scope.updateFood = function (data) {
        var food = angular.copy(data);
        food.$update({}, function () {
            $scope.loadFoods();
            $state.go('app.administration.foods');
        });

    }

    $scope.removeFood = function (data) {
        data.$remove({}, function () {
            $scope.loadFoods();
        });
        $state.go('app.administration.foods');

    }

    $scope.cencelFood = function () {
        $state.go('app.administration.foods');
    }
}