﻿/**
 * @ngInject
 */
function SeparationsController($scope, $stateParams, Separations) {
    $scope.loadSeparations = function () {
        Separations.queryBy({ isArray: true }, function (data) {
            $scope.separations = data;
        });
    }

    $scope.loadSeparations();
}