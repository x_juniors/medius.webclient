﻿/**
 * @ngInject
 */
function ContractsController($scope, $stateParams, Contracts) {
    $scope.getContracts = function () {
        $scope.contracts = Contracts.queryBy({}, function (data) {
            $scope.contracts = data;
        });
    }

    $scope.getContracts();
}