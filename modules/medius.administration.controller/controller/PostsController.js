﻿/**
 * @ngInject
 */
function PostsController($scope, $stateParams, Posts) {
    Posts.queryBy({ isArray: true }, function (data) {
        $scope.posts = data;
    });
}