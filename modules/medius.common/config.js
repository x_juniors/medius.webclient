﻿angular.module('medius.common')
        .service('MediusDateFormatService', MediusDateFormatService)
        .service('ApiService', ApiService)
        .service('DownloadService', DownloadService)
        .factory('RequestDateInterceptor', RequestDateInterceptor)
        .factory('TokenBasedAuthenticationInterceptor', TokenBasedAuthenticationInterceptor)
        ;

angular.module('medius.common').config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('TokenBasedAuthenticationInterceptor');
        $httpProvider.interceptors.push('RequestDateInterceptor');
    }]);