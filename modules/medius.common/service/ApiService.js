﻿/**
 * @ngInject
 */
function ApiService($location) {
    //var baseUrl = $location.protocol()
    //        + '://api.'
    //        + $location.host() + ':' + $location.port()
    //        + '/v1';
    var baseUrl = '/api/api';
    return {
        baseUrl: baseUrl
    };
}
;