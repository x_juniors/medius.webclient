﻿/**
 * @ngInject
 */
function MediusDateFormatService() {
    this.getDate = function (string) {
        var regExp = /(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})/;
        if (regExp.test(string)) {
            var parts = regExp.exec(string);
            var date = new Date(
                (+parts[1]),
                (+parts[2]) - 1, // Month starts at 0
                (+parts[3]),
                (+parts[4]),
                (+parts[5]),
                (+parts[6])
            );
            return date;
        }
        else {
            return null;
        }
    }

    this.getString = function (date) {
        // Objective format: yyyyMMddTHHmmss
        return date.getFullYear().toString()
            + numberToString(date.getMonth() + 1)
            + numberToString(date.getDate())
            + 'T'
            + numberToString(date.getHours())
            + numberToString(date.getMinutes())
            + numberToString(date.getSeconds());
    }

    var numberToString = function (number) {
        if (number < 10) {
            return '0' + number.toString();
        }
        else
            return number.toString();
    }
}