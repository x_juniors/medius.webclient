﻿/**
 * @ngInject
 */
function RequestDateInterceptor(MediusDateFormatService) {

    var parseDateRecursive = function (object) {
        for (var property in object) {
            if (object.hasOwnProperty(property)) {
                if (object[property] instanceof Date) {
                    object[property] = MediusDateFormatService.getString(object[property]);
                }
                else {
                    if (typeof object[property] == "object" && property != "$promise") {
                        parseDateRecursive(object[property]);
                    }
                }
            }
        }
    }

    return {
        request: function (_config) {
            var config = angular.copy(_config);
            parseDateRecursive(config.data);
            return config;
        }
    };
}