﻿/**
 * @ngInject
 */
function TokenBasedAuthenticationInterceptor($cookies) {
    return {
        request: function (config) {
            var token = $cookies.get('token');
            config.headers.Token = token;
            return config;
        },
        responseError: function (response) {
            //if (response.status === 401) {
            //    $cookies.remove('token');
            //    $cookies.remove('role');
            //    $cookies.remove('id');
            //    window.location.reload();
            //}
            return response;
        }
    };
}