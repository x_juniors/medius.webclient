﻿/**
 * @ngInject
 */
function StaffAccountController($scope, $stateParams, $state, Staff, Role, Account) {
    $scope.loadStaffAccount = function () {
        $scope.account = Account.get({ account_id: $stateParams.staff_id });
    }
    $scope.loadRoles = function () {
        $scope.roles = Role.query();
    }
    $scope.loadRoles();
    if ($stateParams.staff_id) {
        $scope.loadStaffAccount();
    }

    $scope.changePassword = function (account) {
        if (account.newPassword !== account.passwordConfirm) {
            alert('Passwords not match!');
        }
        else {
            account.$changePassword({}, function () {
                $scope.loadStaffAccount();
            });
        }
    }

    $scope.update = function (data) {
        var account = angular.copy(data);
        delete account.role;
        account.$update();
    }

    $scope.addEmployee = function (account, employee) {
        if (account.passwordHash !== account.passwordConfirm) {
            alert('Passwords not match!');
        }
        else {
            Account.save({}, account, function (response) {
                employee.id = response.id;
                Staff.save({}, employee, function (response) {
                    $state.go('app.employee', { staff_id: response.id });
                });
            });
        }
    }
}