/**
 * @ngInject
 */
function StaffListController($scope, Staff) {
    $scope.loadStaff = function () {
        $scope.staff = Staff.query();
    }

    $scope.loadStaff();
}