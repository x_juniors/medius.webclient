/**
 * @ngInject
 */
function StaffController($scope, $stateParams, $state, Staff) {
    $scope.loadEmployee = function () {
        if ($stateParams.staff_id) {
            $scope.employee = Staff.get({ staff_id: $stateParams.staff_id });
        }
    }
    $scope.loadEmployee();

    $scope.saveEmployee = function (data) {
        data.$update();
    }

    $scope.removeEmployee = function (data) {
        data.$remove({}, function () {
            $state.go('app.staff');
        });
    }
}