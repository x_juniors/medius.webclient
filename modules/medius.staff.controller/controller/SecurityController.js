﻿/**
 * @ngInject
 */
function SecurityController($scope, $cookies, Token) {

    $scope.loadTokens = function () {
        $scope.tokens = Token.queryBy({ account_id: $cookies.get('id') });
    }
    $scope.loadTokens();

    $scope.getStatusClass = function (token) {
        var isOld = token.expiresIn < Date.now()
        if (isOld) {
            return 'danger';
        }
    }

    $scope.clearAllTokensExcept = function (tokens) {
        Token.clearExcept({ account_id: $cookies.get('id') }, function (data) {
            $scope.loadTokens();
        });
    }
}