﻿/**
 * @ngInject
 */
function AccountController($scope, $cookies, $state, AclService, Token) {
    $scope.can = AclService.can;

    $scope.signIn = function (credential) {
        Token.authenticate({}, credential, function (data) {
            if (data.token) {
                var expires;
                if ($scope.isRemember) {
                    expires = data.token.expiresIn;
                }
                else {
                    expires = null;
                }
                $cookies.put('token', data.token.value, { expires: expires });
                $cookies.put('role', data.role, { expires: expires });
                $cookies.put('id', data.accountId, { expires: expires });
                AclService.attachRole(data.role);
                AclService.detachRole('guest');
                $state.go('app');
            }
            else {
                alert(data.message);
                $scope.credential.passwordHash = '';
            }
        });
    }

    $scope.signOut = function () {
        Token.remove({ token_id: $cookies.get('token') }, function (data) {
            AclService.detachRole($cookies.get('role'));
            AclService.attachRole('guest');
            $cookies.remove('token');
            $cookies.remove('role');
            $cookies.remove('id');
            $state.go('sign_in');
        })
    }

    $scope.isActive = function (state) {
        if ($state.is(state)) {
            return 'active';
        }
    }
}