﻿/**
 * @ngInject
 */
function ImController($scope, $cookies, Account) {
    
    $scope.loadAccount = function () {
        $scope.account = Account.get({ account_id: $cookies.get('id') });
        $scope.currentRole = $cookies.get('role');
    }
    $scope.loadAccount();

    $scope.changePassword = function (account) {
        if (account.newPassword === account.passwordConfirm) {
            account.$changePassword({}, function (response) {
                alert('OK');
                $scope.loadAccount();
            });
        }
        else {
            alert('Passwords not match!');
        }
    }
}