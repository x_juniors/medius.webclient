angular.module('medius.icd').config(function ($stateProvider) {
    var templateDir = 'modules/medius.icd/template/';

    $stateProvider
            .state('app.icd', {
                url: "/icd",
                templateUrl: templateDir + "icd.html",
                controller: IcdClassController
            })
            .state('app.class', {
                url: "/icd/classes/:class_id",
                templateUrl: templateDir + "class.html"
            })
            .state('app.block', {
                url: "/icd/blocks/:block_id",
                templateUrl: templateDir + "block.html"
            })
            .state('app.search', {
                url: "/icd/search/:expression",
                templateUrl: templateDir + "search.html"
            })
            ;
});