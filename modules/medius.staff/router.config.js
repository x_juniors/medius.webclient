angular.module('medius.staff').config(function ($stateProvider) {
    var templateDir = 'modules/medius.staff/template/';
    $stateProvider
            .state('app.staff', {
                url: "/staff",
                templateUrl: templateDir + "staff.html",
                controller: StaffListController
            })
            .state('app.new', {
                url: "/staff/new",
                templateUrl: templateDir + "new.html",
                controller: StaffAccountController
            })
            .state('app.employee', {
                url: "/staff/:staff_id",
                templateUrl: templateDir + "employee.html",
                controller: StaffController
            })
            .state('app.employee.account', {
                url: "/account",
                templateUrl: templateDir + "account.html",
                controller: StaffAccountController
            })
            ;
});