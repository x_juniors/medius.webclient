﻿/**
 * @ngInject
 */
function TemperaturesController($scope, $stateParams, $state, Temperature, MediusDateFormatService) {
    // Dictionary key=date as string, value=temperature resource
    $scope.temperatureDict = [];
    $scope.series = ['Температура'];
    // Default dot
    $scope.labels = [''];
    $scope.data = [['']];

    $scope.onClick = function (points, evt) {
        var id = MediusDateFormatService.getString($scope.temperatureDict[points[0].label].date);
        $state.go('app.medcard.temperature_list.temperatures.temperature', { temperature_id: id });
    };

    $scope.loadTemperatures = function () {
        Temperature.queryBy({ medcard_id: $stateParams.medcard_id }, function (temperatures) {
            var datetimes = [];
            var values = [];
            for (var i = 0; i < temperatures.length; i++) {
                var date = temperatures[i].date.toLocaleString();
                $scope.temperatureDict[date] = temperatures[i];
                datetimes.push(date);
                values.push(temperatures[i].value);
            }
            $scope.labels = datetimes;
            $scope.data = [values];
        });
    }
    $scope.loadTemperatures();
}