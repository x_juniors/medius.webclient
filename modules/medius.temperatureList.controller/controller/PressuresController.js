﻿/**
 * @ngInject
 */
function PressuresController($scope, $stateParams, $state, Pressure, MediusDateFormatService) {
    // Dictionary key=date as string, value=temperature resource
    $scope.dictionary = [];
    $scope.series = ['Тиск систолічний', 'Тиск діастолічний'];
    // Default dot
    $scope.labels = [''];
    $scope.data = [[''], ['']];

    $scope.onClick = function (points, evt) {
        var id = MediusDateFormatService.getString($scope.dictionary[points[0].label].date);
        $state.go('app.medcard.temperature_list.pressures.pressure', { pressure_id: id });
    };

    $scope.loadPressures = function () {
        Pressure.queryBy({ medcard_id: $stateParams.medcard_id }, function (pressures) {
            var datetimes = [];
            var values = [];
            var values2 = [];
            for (var i = 0; i < pressures.length; i++) {
                var date = pressures[i].date.toLocaleString();
                $scope.dictionary[date] = pressures[i];
                datetimes.push(date);
                values.push(pressures[i].top);
                values2.push(pressures[i].lower);
            }
            $scope.labels = datetimes;
            $scope.data = [values, values2];
        });
    }
    $scope.loadPressures();
}