﻿/**
 * @ngInject
 */
function TemperatureController($scope, $stateParams, $state, Temperature, MediusDateFormatService) {
    if ($stateParams.temperature_id != null) {
        // New
        $scope.temperature = Temperature.get({ temperature_id: $stateParams.temperature_id });
    }
    else {
        // Update
        $scope.temperature = new Temperature();
        $scope.temperature.date = new Date("");
    }

    $scope.addTemperature = function (temperature) {
        Temperature.save({ medcard_id: $stateParams.medcard_id }, temperature, function () {
            $scope.loadTemperatures();
        });
    }

    $scope.updateTemperature = function (temperature) {
        temperature.$update({ temperature_id: $stateParams.temperature_id }, function () {
            $scope.loadTemperatures();
            $state.go('^');
        });
    }

    $scope.removeTemperature = function (temperature) {
        Temperature.remove({ temperature_id: $stateParams.temperature_id }, function () {
            $scope.loadTemperatures();
            $state.go('^');
        });
    }
}