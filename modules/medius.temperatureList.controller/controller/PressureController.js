﻿/**
 * @ngInject
 */
function PressureController($scope, $stateParams, $state, Pressure, MediusDateFormatService) {
    if ($stateParams.pressure_id != null) {
        // New
        $scope.pressure = Pressure.get({ pressure_id: $stateParams.pressure_id });
    }
    else {
        // Update
        $scope.pressure = new Pressure();
        $scope.pressure.date = new Date("");
    }

    $scope.addPressure = function (pressure) {
        Pressure.save({ medcard_id: $stateParams.medcard_id }, pressure, function () {
            $scope.loadPressures();
        });
    }

    $scope.updatePressure = function (pressure) {
        pressure.$update({ pressure_id: $stateParams.pressure_id }, function () {
            $scope.loadPressures();
            $state.go('^');
        });
    }

    $scope.removePressure = function (pressure) {
        Pressure.remove({ pressure_id: $stateParams.pressure_id }, function () {
            $scope.loadPressures();
            $state.go('^');
        });
    }
}