﻿/**
 * @ngInject
 */
function PulsesController($scope, $stateParams, $state, Pulse, MediusDateFormatService) {
    // Dictionary key=date as string, value=temperature resource
    $scope.dictionary = [];
    $scope.series = ['Пульс'];
    // Default dot
    $scope.labels = [''];
    $scope.data = [['']];

    $scope.onClick = function (points, evt) {
        var id = MediusDateFormatService.getString($scope.dictionary[points[0].label].date);
        $state.go('app.medcard.temperature_list.pulses.pulse', { pulse_id: id });
    };

    $scope.loadPulses = function () {
        Pulse.queryBy({ medcard_id: $stateParams.medcard_id }, function (pulses) {
            var datetimes = [];
            var values = [];
            for (var i = 0; i < pulses.length; i++) {
                var date = pulses[i].date.toLocaleString();
                $scope.dictionary[date] = pulses[i];
                datetimes.push(date);
                values.push(pulses[i].value);
            }
            $scope.labels = datetimes;
            $scope.data = [values];
        });
    }
    $scope.loadPulses();
}