﻿/**
 * @ngInject
 */
function PulseController($scope, $stateParams, $state, Pulse, MediusDateFormatService) {
    if ($stateParams.pulse_id != null) {
        // New
        $scope.pulse = Pulse.get({ pulse_id: $stateParams.pulse_id });
    }
    else {
        // Update
        $scope.pulse = new Pulse();
        $scope.pulse.date = new Date("");
    }

    $scope.addPulse = function (pulse) {
        Pulse.save({ medcard_id: $stateParams.medcard_id }, pulse, function () {
            $scope.loadPulses();
        });
    }

    $scope.updatePulse = function (pulse) {
        pulse.$update({ pulse_id: $stateParams.pulse_id }, function () {
            $scope.loadPulses();
            $state.go('^');
        });
    }

    $scope.removePulse = function (pulse) {
        Pulse.remove({ pulse_id: $stateParams.pulse_id }, function () {
            $scope.loadPulses();
            $state.go('^');
        });
    }
}