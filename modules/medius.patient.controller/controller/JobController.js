﻿/**
 * @ngInject
 */
function JobController($scope, $stateParams, Organization, Job) {

    $scope.loadJob = function () {
        Job.getBy({ patient_id: $stateParams.patient_id }, function (data) {
            if (data['id']) {
                $scope.job = data;
            }
        })

        Organization.getBy({ patient_id: $stateParams.patient_id }, function (data) {
            if (data['id']) {
                $scope.organization = data;
            }
        })
    }
    $scope.loadJob();

    $scope.getOrganizations = function (expression) {
        $scope.job = null;
        return Organization.queryFilter({ expression: expression }).$promise;
    }

    $scope.getJobs = function (expression) {
        return Job.queryFilter({ organization_id: $scope.organization.id, expression: expression }).$promise;
    }

    $scope.saveJob = function () {
        if ($scope.job !== null) {
            if ($scope.job.id == null) {
                // string
                var job = new Job();
                job.post = $scope.job;
                job.organizationId = $scope.organization.id;
                job.$save({}, function (data) {
                    $scope.job = data;
                    $scope.patient.jobId = data.id;
                    $scope.patient.$updateJob();
                });
            }
            else {
                // Street object
                $scope.patient.jobId = $scope.job.id;
                $scope.patient.$updateJob();
            }
        }
        else {
            $scope.patient.jobId = null;
            $scope.patient.$updateJob();
        }
    }
}