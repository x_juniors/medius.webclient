﻿/**
 * @ngInject
 */
function LocationsController($scope, $stateParams, Location, Region, Locality, Street) {

    $scope.loadLocation = function (id) {
        Location.get({ patient_id: id }, function (data) {
            $scope.location = data;
            loadLocationDepends(id);
        });
    };
    $scope.loadLocation($stateParams.patient_id);

    var loadLocationDepends = function (id) {
        Region.getBy({ patient_id: id }, function (data) {
            if(data['id']){
                $scope.location.region = data;
            }
        });
        Locality.getBy({ patient_id: id }, function (data) {
            if (data['id']) {
                $scope.location.locality = data;
            }
        });
        Street.getBy({ patient_id: id }, function (data) {
            if (data['id']) {
                $scope.location.street = data;
            }
        });
    };

    $scope.getRegions = function (expression) {
        if ($scope.location != null) {
            $scope.location.street = null;
            $scope.location.locality = null;
        }
        return Region.queryFilter({ expression: expression }).$promise;
    }

    $scope.getLocalities = function (expression) {
        if ($scope.location != null)
            $scope.location.street = null;
        return Locality.queryFilter({ region_id: $scope.location.region.id, expression: expression }).$promise;
    }

    $scope.getStreets = function (expression) {
        return Street.queryFilter({ locality_id: $scope.location.locality.id, expression: expression }).$promise;
    }

    $scope.saveLocation = function (location) {
        location.locality.$update();
        if (location.street !== null) {
            if (location.street.id == null) {
                // string
                var street = new Street();
                street.title = location.street;
                street.localityId = location.locality.id;
                street.$save({}, function (data) {
                    location.street = data;
                    location.streetId = data.id;
                    location.$update({ patient_id: $stateParams.patient_id });
                });
            }
            else {
                // Street object
                location.streetId = location.street.id;
                location.$update({ patient_id: $stateParams.patient_id });
            }
        }
        else {
            location.streetId = null;
            location.$update({ patient_id: $stateParams.patient_id });
        }
    }

    //$scope.addLocation = function () {
    //    var location = new Location();
    //    location.patientId = $stateParams.patient_id;
    //    location.$save({}, function (data) {
    //        $scope.locations.push(data);
    //    });
    //}

}