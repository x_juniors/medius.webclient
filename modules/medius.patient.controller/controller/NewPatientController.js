/**
 * @ngInject
 */
function NewPatientController($scope, $stateParams, $state, Patient, Category) {
    $scope.loadNewPatient = function() {
        $scope.newPatient = {
            lastname: null,
            name: null,
            patronymic: null,
            sensitivity: null,
            sex: null,
            born: null,
            bloodType: null,
            rhFactor: null,
            category: null
        }
        
    }

    $scope.loadNewPatient();

    $scope.saveNewPatient = function (patient) {
        var newPatient = angular.copy(patient);

       if (newPatient.category !== null) {
            if (newPatient.category.id == null) {
                // string
                var category = new Category();
                category.title = newPatient.category;
                category.$save({}, function (data) {
                    newPatient.category = data;
                    newPatient.categoryId = data.id;
                    Patient.save({ patient_id: $stateParams.patient_id }, newPatient, function (response) {
                         $state.go('app.patient', { patient_id: response.id });
                    });
                });
                
            }
            else {
                // Street object
                newPatient.categoryId = newPatient.category.id;
                Patient.save({ patient_id: $stateParams.patient_id }, newPatient, function (response) {
                    $state.go('app.patient', { patient_id: response.id });
                });
            }
        }
        else {
           newPatient.categoryId = null;
           Patient.save({ patient_id: $stateParams.patient_id }, newPatient, function (response) {
               $state.go('app.patient', { patient_id: response.id });
            });
        }
           
    }

    $scope.cencelNewPatient = function () {
        $state.go('app.patients');
    }

    $scope.getCategories = function (expression) {
        return Category.queryFilter({ expression: expression }).$promise;
    }
}