/**
 * @ngInject
 */
function PatientListController($scope, Patient) {
    $scope.loadPatients = function() {
        $scope.patients = Patient.query();
    }

    $scope.loadPatients();
}