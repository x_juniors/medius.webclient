/**
 * @ngInject
 */
function PatientController($scope, $stateParams, $state, Patient, Medcard, Category) {
    $scope.loadPatient = function () {
        if ($stateParams.patient_id) {
            $scope.patientId = $stateParams.patient_id;
            $scope.patient = Patient.get({patient_id: $stateParams.patient_id}, function () {
                Category.getBy({patient_id: $stateParams.patient_id}, function (data) {
                    if (data['id']) {
                        $scope.patient.category = data;
                    }
                });
            });
        }
    };

    $scope.loadPatient();

    $scope.savePatient = function (patient) {
        if (patient.category !== null) {
            if (patient.category.id == null) {
                // string
                var category = new Category();
                category.title = patient.category;
                category.$save({}, function (data) {
                    patient.category = data;
                    patient.categoryId = data.id;
                    patient.$update();
                });
            }
            else {
                // object
                patient.categoryId = patient.category.id;
                patient.$update();
            }
        }
        else {
            patient.categoryId = null;
            patient.$update();
        }
    };

    $scope.removePatient = function (patient) {
        patient.$remove({}, function () {
            $state.go('app.patients');
        });
    }

    $scope.loadPatientMedcards = function () {
        $scope.patient_medcards = Medcard.queryBy({patient_id: $stateParams.patient_id});
    }

    $scope.getCategories = function (expression) {
        return Category.queryFilter({expression: expression}).$promise;
    }
}