angular.module('medius.usd').config(function ($stateProvider) {
    var templateDir = 'modules/medius.usd/template/';

    $stateProvider
            .state('app.patient.newUsd', {
                url: "/usd/new",
                templateUrl: templateDir + "new.html",
                controller: AbdomenUsdController
            })
            .state('app.patient.usd', {
                url: "/usd/:usd_id",
                views: {
                    'leftNav': {
                        templateUrl: templateDir + "_nav.html"
                    },
                    'patientChild': {
                        templateUrl: templateDir + "usd.html",
                        controller: AbdomenUsdController
                    }
                }
            })
            .state('app.patient.usd.liver', {
                url: "/liver",
                templateUrl: templateDir + "liver.html"
            })
            .state('app.patient.usd.liver.liverMain', {
                url: "/main",
                templateUrl: templateDir + "liver_main.html",
                controller: LiverMainController
            })
            .state('app.patient.usd.liver.liverVolumeProcess', {
                url: "/volume-process",
                templateUrl: templateDir + "liver_volume_process.html",
                controller: LiverVolumeProcessController
            })
            .state('app.patient.usd.gallbladder', {
                url: "/gallbladder",
                templateUrl: templateDir + "gallbladder.html",
                controller: GallbladderController
            })
            .state('app.patient.usd.pancreas', {
                url: "/pancreas",
                templateUrl: templateDir + "pancreas.html",
                controller: PancreasController
            })
            .state('app.patient.usd.kidneys', {
                url: "/kidneys",
                templateUrl: templateDir + "kidneys.html"
            })
            .state('app.patient.usd.kidneys.left', {
                url: "/left",
                templateUrl: templateDir + "left_kidney.html",
                controller: LeftKidneyController
            })
            .state('app.patient.usd.kidneys.right', {
                url: "/right",
                templateUrl: templateDir + "right_kidney.html",
                controller: RightKidneyController
            })
            .state('app.patient.usd.spleen', {
                url: "/spleen",
                templateUrl: templateDir + "spleen.html",
                controller: SpleenController
            })
            .state('app.patient.usd.adrenals', {
                url: "/adrenals",
                templateUrl: templateDir + "adrenals.html",
                controller: AdrenalsController
            })
    ;
});