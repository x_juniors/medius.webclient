angular.module('medius.patient').config(function ($stateProvider) {
    var templateDir = 'modules/medius.patient/template/';
    $stateProvider
            .state('app.patients', {
                url: "/patients",
                templateUrl: templateDir + "patients.html",
                controller: PatientListController
            })
            .state('app.newPatient', {
                url: "/patients/new",
                templateUrl: templateDir + "new.html",
                controller: NewPatientController
            })
            .state('app.patient', {
                url: "/patients/:patient_id",
                templateUrl: templateDir + "patient.html",
                controller: PatientController
            })
            .state('app.patient.main', {
                url: "/main",
                templateUrl: templateDir + "main.html"
            })
            .state('app.patient.locations', {
                url: "/locations",
                templateUrl: templateDir + "locations.html",
                controller: LocationsController
            })
            .state('app.patient.contacts', {
                url: "/contacts",
                templateUrl: templateDir + "contacts.html"
            })
            .state('app.patient.job', {
                url: "/job",
                templateUrl: templateDir + "job.html",
                controller: JobController
            })
            .state('app.patient.medcards', {
                url: "/medcards",
                templateUrl: templateDir + "medcards.html"
            })
            .state('app.patient.usds', {
                url: "/usds",
                templateUrl: templateDir + "usd.html",
                controller: AbdomenUsdListController
            })
            ;
});