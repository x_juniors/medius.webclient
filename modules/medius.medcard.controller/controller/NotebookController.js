﻿/**
 * @ngInject
 */
function NotebookController($scope, $state, $stateParams, Notebooks) {
    $scope.loadNotebook = function() {
        if ($stateParams.notebook_id) {
            $scope.note= Notebooks.getBy({ notebook_id: $stateParams.notebook_id });
        }
    }

    $scope.loadNotebook();

    $scope.updateNotebook = function (data) {
        var note = angular.copy(data);
        note.$update({},function() {
            $scope.loadNotebooks();
            $state.go('app.medcard.treatment.notes');
        });

    }

    $scope.removeNotebook = function (data) {
        data.$remove({}, function () {
            $scope.loadNotebooks();
        });
        $state.go('app.medcard.treatment.notes');
    }

    $scope.cencelNotebook = function () {
        $state.go('app.medcard.treatment.notes');
    }
}