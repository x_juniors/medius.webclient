﻿/**
 * @ngInject
 */
function ExpertInspectionController($scope, $state, $stateParams, ExpertInspections) {
    $scope.isUpdate = null;
    $scope.expertinspection = {
        date: null,
        note: null,
        author: null,
        id: null,
        medcardId: null
    }

    if ($stateParams.expertinspection_id) {
        $scope.expertinspectionw = ExpertInspections.getBy({ expertinspections_id: $stateParams.expertinspection_id }, function (data) {
            if (data.date != null)
                data.date = new Date(data.date);

            $scope.expertinspection.date = data.date;
            $scope.expertinspection.note = data.note;
            $scope.expertinspection.author = data.author;
            $scope.expertinspection.id = data.id;
            $scope.expertinspection.medcardId = data.medcardId;
        });


    } 

    $scope.back = function () {
        $state.go('app.medcard.treatment.expert_inspections');
    }

    $scope.update = function () {

        ExpertInspections.update({ medcard_id: $scope.expertinspection.medcardId }, $scope.expertinspection, function (data) {
            $scope.reload();
            $state.go('app.medcard.treatment.expert_inspections');
        });
    }


    $scope.delete = function () {

        ExpertInspections.remove({ expertinspections_id: $scope.expertinspection.id }, function () {
            $scope.reload();
            $state.go('app.medcard.treatment.expert_inspections');
        });

    }



}