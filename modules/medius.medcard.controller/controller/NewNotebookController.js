﻿/**
 * @ngInject
 */
function NewNotebookController($scope, $state, $stateParams, Notebooks) {
    $scope.loadNewNotebook = function() {
        $scope.newNote = {
            note: null,
            date: null
        }
    }

    $scope.loadNewNotebook();

    $scope.saveNotebook = function(data) {
        if ($scope.newNote.note != null && $scope.newNote.date != null) {
          
            var note = angular.copy(data);
            note.date = new Date(note.date);

            Notebooks.save({medcard_id: $stateParams.medcard_id}, note, function() {
                $scope.loadNotebooks();
                $state.go('app.medcard.treatment.notes');
            });
        } else alert("Insert all data!!! Please!");
    }

    $scope.cencelNotebook = function () {
        $state.go('app.medcard.treatment.notes');
    }
}