/**
 * @ngInject
 */
function HospitalizationController($scope, $stateParams, Hospitalization) {
    $scope.loadHospitalization = function () {
        if ($stateParams.medcard_id) {
            $scope.hospitalization = Hospitalization.getBy({ medcard_id: $stateParams.medcard_id });
        }
    }

    $scope.loadHospitalization();

    $scope.updateHospitalization = function (data) {
        data.$update();
    }
}