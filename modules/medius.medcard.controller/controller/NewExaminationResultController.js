﻿/**
 * @ngInject
 */
function NewExaminationResultController($scope, $state, $stateParams, ExaminationResults) {
    $scope.loadNewResult = function() {
        $scope.newResult = {
            data: null,
            note: null
        }
    }

    $scope.loadNewResult();

    $scope.saveNewResult = function(data){
        if (data.date != null && data.note != null) {
            ExaminationResults.save({ medcard_id: $stateParams.medcard_id }, data, function () {
                $state.go('app.medcard.treatment.examination_results');
                $scope.loadExaminationResults();
            });
        } else alert("Insert all data!!! Please!");
    }

    $scope.cencelNewResult = function() {
        $state.go('app.medcard.treatment.examination_results');
    }
}