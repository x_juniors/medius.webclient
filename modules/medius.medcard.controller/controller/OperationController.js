﻿/**
 * @ngInject
 */
function OperationController($scope, $stateParams, $state, Operation) {
    $scope.loadOperations = function() {
        if ($stateParams.medcard_id) {
            $scope.operations = Operation.queryBy({ medcard_id: $stateParams.medcard_id });
            console.log(Operation.queryBy({ medcard_id: $stateParams.medcard_id }));
        }
    }

    $scope.loadOperations();

    if ($stateParams.operation_id) {
        Operation.get({ operation_id: $stateParams.operation_id }, function (data) {
            $scope.operation = data;
        });
        //$scope.operation.date = Date.now();
        //console.log($scope.operation);
    }

    $scope.updateOperation = function (data) {
        var operation = angular.copy(data);
        //console.log(operation);
        
        operation.$update();
    }


    $scope.deleteOperation = function (data) {
        bootbox.confirm("Ви впевнені?", function (result) {
            if (result) {
                var operation = angular.copy(data);
                //console.log(operation);

                operation.$remove();
                $scope.refresh();
                $state.go('app.medcard.treatment.operations', {
                    medcard_id: $scope.medcard.id
                });
            }
        });
        
    }

    $scope.refresh = function() {
        if ($stateParams.operation_id) {
            Operation.get({ operation_id: $stateParams.operation_id }, function (data) {
                $scope.operation = data;
            });
            //$scope.operation.date = Date.now();
            //console.log($scope.operation);
        }
    }
}