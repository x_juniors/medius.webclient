﻿/**
 * @ngInject
 */
function ExpertInspectionNewController($scope, $state, $stateParams, ExpertInspections) {
    $scope.expertinspectionNew = {
        date: null,
        note: null,
        author: null,
        medcardId: null
    }


    $scope.backNew = function () {
        $state.go('app.medcard.treatment.expert_inspections');
    }

    $scope.addExpertInspection = function () {
        ExpertInspections.save({ medcard_id: $stateParams.medcard_id }, $scope.expertinspectionNew, function (data) {
            $scope.expertinspections.push(data);
            $state.go('app.medcard.treatment.expert_inspections', {
                medcard_id: $stateParams.medcard_id
            });
        });
    }
}