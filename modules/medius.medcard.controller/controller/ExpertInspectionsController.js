﻿/**
 * @ngInject
 */
function ExpertInspectionsController($scope, $stateParams, ExpertInspections) {
    if ($stateParams.medcard_id) {
        $scope.expertinspections = ExpertInspections.queryBy({ medcard_id: $stateParams.medcard_id, isArray: true });
    }


    $scope.reload = function() {
        if ($stateParams.medcard_id) {
            $scope.expertinspections = ExpertInspections.queryBy({ medcard_id: $stateParams.medcard_id, isArray: true });
        }
    }

}