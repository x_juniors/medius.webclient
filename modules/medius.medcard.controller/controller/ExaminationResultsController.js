﻿/**
 * @ngInject
 */
function ExaminationResultsController($scope, $stateParams, ExaminationResults) {
    $scope.loadExaminationResults = function (){
        if ($stateParams.medcard_id) {
            $scope.examinationresults =  ExaminationResults.queryBy({ medcard_id: $stateParams.medcard_id });
        }
    }
    $scope.loadExaminationResults();
}