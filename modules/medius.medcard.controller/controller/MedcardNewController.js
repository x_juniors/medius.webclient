﻿/**
 * @ngInject
 */
function MedcardNewController($scope, $stateParams, $state, Medcard, Separations) {
    $scope.currentYear = new Date().getFullYear() + "-";
    //$scope.numberShort = null;
    $scope.medcard = {
        //number: null,
        //hospitalizationDate: null,
        //chamber: null,
        //isHospitalizedInCurrentYear: null,
        //pediculosis: null,
        //hepatitisStatus: null,
        //hepatitisDate: null,
        //scabInspection: null,
        //pulseValue: null,
        //temperatureValue: null,
        //pressureTop: null,
        //pressureLower: null,
        //rw: null,
        //rwStatus: null,
        //hivInfection: null,
        //hivStatus: null,
        //sender: null,
        //food: null

    }

    if ($stateParams.patient_id)
        //alert($scope.currentYear);
    {
        Separations.queryBy({ isArray: true }, function (data) {
            $scope.separations = data;
        });
    }

    $scope.addMedcard = function () {
        //alert($scope.medcard.shortNumber);
        $scope.medcard.number = $scope.currentYear + $scope.medcard.shortNumber;
        Medcard.save({ patient_id: $stateParams.patient_id }, $scope.medcard, function (data) {
            
            $state.go('app.medcard', {
                medcard_id: data.id
            });
        });
    }
}
