﻿/**
 * @ngInject
 */
function AdmissionDepartmentController($scope, $stateParams, AdmissionDepartment) {
    //if ($stateParams.status === "404") {
    //    console.log("ALL wrong");
    //}
    var status = null;

    $scope.loadAdmissionDepartment = function () {
        if ($stateParams.medcard_id) {
            AdmissionDepartment.queryBy({ medcard_id: $stateParams.medcard_id }, function (data) {
                $scope.admissionDepartment = data;

            }, function (response) {
                //console.log(response.status );
                status = response.status;
            });
        }
    }

    $scope.loadAdmissionDepartment();


    $scope.updateAdmissionDepartment = function (data) {
        var admissionDepartment = angular.copy(data);
        //update();
        if (status != 404) {
            admissionDepartment.$update();
        } else {

            AdmissionDepartment.save({ medcard_id: $stateParams.medcard_id }, $scope.admissionDepartment);
        }

    }
}