/**
 * @ngInject
 */
function MedcardListController($scope, Medcard) {
    $scope.medcards = Medcard.query();
}