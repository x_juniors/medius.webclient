/**
 * @ngInject
 */
function TreatmentController($scope, $stateParams, Treatment, TreatmentDiagnose) {
    $scope.loadTreatment = function () {
        if ($stateParams.medcard_id) {
            $scope.treatment = Treatment.queryBy({ medcard_id: $stateParams.medcard_id });
            $scope.treatmentDiagnose = TreatmentDiagnose.queryBy({ medcard_id: $stateParams.medcard_id });
        }
    }
    $scope.loadTreatment();

    $scope.updateTreatment = function (treatment, treatmentDiagnose) {
        treatment.$update();
        treatmentDiagnose.$update();
    }
}