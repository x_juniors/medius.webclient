﻿/**
 * @ngInject
 */
function ExaminationResultController($scope, $state, $stateParams, ExaminationResults) {
    $scope.loadExaminationResult = function () {
        if ($stateParams.examinationresult_id) {
            ExaminationResults.getBy({ examinationresult_id: $stateParams.examinationresult_id }, function (data) {
                data.date = new Date(data.date);
                $scope.result = data;
            });
        }
    }

    $scope.loadExaminationResult();

    $scope.updateExaminationResult = function (data) {
        var result = angular.copy(data);
        result.data = new Date(result.date);
        result.$update({}, function () {
            $scope.loadExaminationResults();
            $state.go('app.medcard.treatment.examination_results');
        });

    }

    $scope.removeExaminationResult = function (data) {
        data.$remove({}, function () {
            $scope.loadExaminationResults();
        });
        $state.go('app.medcard.treatment.examination_results');
    }

    $scope.cencelExaminationResult = function () {
        $state.go('app.medcard.treatment.examination_results');
    }
}