﻿/**
 * @ngInject
 */
function MedcardController($scope, $stateParams, $state, Medcard, DownloadService, Separations, ApiService) {

    $scope.back = function () {
        $state.go('app.patient', {
            patient_id: $scope.medcard.patientId
        });
    }

    if ($stateParams.medcard_id) {
        Medcard.get({ medcard_id: $stateParams.medcard_id }, function (data) {
            data.numberYear = data.number.substr(0, 5);
            data.numberNumber = data.number.substr(5);
            $scope.medcard = data;
        });
        Separations.queryBy({ isArray: true }, function (data) {
            $scope.separations = data;
        });
    }

    $scope.updateMedcard = function (data) {
        var medcard = angular.copy(data);
        medcard.number = medcard.numberYear + medcard.numberNumber;
        medcard.$update();
    }

    $scope.deleteMedcard = function (data) {
        bootbox.confirm("Ви впевнені?", function (result) {
            if (result) {
                var medcard = angular.copy(data);
                medcard.$remove();
            }
        });

    }

    $scope.refresh = function () {
        if ($stateParams.medcard_id) {
            Medcard.get({ medcard_id: $stateParams.medcard_id }, function (data) {
                data.numberYear = data.number.substr(0, 5);
                data.numberNumber = data.number.substr(5);
                $scope.medcard = data;
            });
        }
    }

    $scope.downloadMedcard = function () {
        var httpPath = ApiService.baseUrl + '/medcards/' + $stateParams.medcard_id + '/download';
        DownloadService.download(httpPath);
    }

    $scope.toPatient = function () {
        console.log($scope.medcard);
        $state.go('app.patient', { patient_id: $scope.medcard.patientId });
    }
}
