﻿/**
 * @ngInject
 */
function EfficiencyMarkController($scope, $state, $stateParams, EfficiencyMarks) {

    $scope.isUpdate = null;
    $scope.efficiencymark = {
        end: null,
        begin: null,
        number: null,
        id: null,
        medcardId: null
    }

    if ($stateParams.efficiencymark_id) {
        $scope.efficiencymarkw = EfficiencyMarks.getBy({ efficiencymarks_id: $stateParams.efficiencymark_id }, function(data) {
            $scope.efficiencymark.begin = data.begin;
            $scope.efficiencymark.end = data.end;
            $scope.efficiencymark.number = data.number;
            $scope.efficiencymark.id = data.id;
            $scope.efficiencymark.medcardId = data.medcardId;
            $scope.isUpdate = true;

        });

        //$scope.efficiencymarks = EfficiencyMarks.queryBy({ medcard_id: $stateParams.medcard_id, isArray: true });

    } else {
        $scope.isUpdate = false;
        $scope.efficiencymark.begin = null;
        $scope.efficiencymark.end = null;
        $scope.efficiencymark.number = null;
        $scope.efficiencymark.id = null;
        $scope.efficiencymark.medcardId = data.medcardId;
    }

    $scope.back = function() {
        $state.go('^');
    }

    $scope.update = function () {

        EfficiencyMarks.update({ medcard_id: $scope.efficiencymark.medcardId }, $scope.efficiencymark, function (data) {
            $scope.reload();
            $state.go('app.medcard.treatment.efficiency_marks', {
                medcard_id: $scope.efficiencymark.medcardId
            });
        });
    }


    $scope.delete = function () {

        EfficiencyMarks.remove({ efficiencymarks_id: $scope.efficiencymark.id }, function() {
            $scope.reload();
            $state.go('app.medcard.treatment.efficiency_marks', {
                medcard_id: $scope.efficiencymark.medcardId
            });
        });
       
    }


    


    



}