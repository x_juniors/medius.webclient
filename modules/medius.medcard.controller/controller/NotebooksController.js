﻿/**
 * @ngInject
 */
function NotebooksController($scope, $stateParams, Notebooks) {
    $scope.loadNotebooks = function() {
        if ($stateParams.medcard_id) {
            $scope.notebooks = Notebooks.queryBy({ medcard_id: $stateParams.medcard_id });
        }
    }
    $scope.loadNotebooks();
}