﻿/**
 * @ngInject
 */
function EfficiencyMarksController($scope, $stateParams, EfficiencyMarks) {
    $scope.medcardId = $stateParams.medcard_id;
    $scope.efficiencymark = {
        end: null,
        begin: null,
        number: null
    }
    $scope.null = null;
    if ($stateParams.medcard_id) {
        $scope.efficiencymarks = EfficiencyMarks.queryBy({ medcard_id: $stateParams.medcard_id, isArray: true });
    }


    

    $scope.reload = function() {
        if ($stateParams.medcard_id) {
            $scope.efficiencymarks = EfficiencyMarks.queryBy({ medcard_id: $stateParams.medcard_id, isArray: true });

        }
    }





}