﻿/**
 * @ngInject
 */
function EfficiencyMarkNewController($scope, $state, $stateParams, EfficiencyMarks) {
    $scope.efficiencymarkNew = {
        end: null,
        begin: null,
        number: null,
        //id: null,
        medcardId: null
    }

    if ($stateParams.medcard_id) {
        //alert("all ok");
        //$scope.efficiencymarkNew = $stateParams.medcard_id;
    }

    $scope.backNew = function () {
        $state.go('app.medcard.treatment.efficiency_marks');
    }

    $scope.addEfficiencymark = function (data) {
        //$scope.efficiencymarkNew.begin = $scope.efficiencymarkNew.begin.toString().substr(0, 21);
        //$scope.efficiencymarkNew.end = $scope.efficiencymarkNew.end.toString().substr(0, 21);
        EfficiencyMarks.save({ medcard_id: $stateParams.medcard_id }, $scope.efficiencymarkNew, function (data) {
            //console.log(data);
            //$scope.reload();
            $scope.efficiencymarks.push(data);
            $state.go('app.medcard.treatment.efficiency_marks', {
                medcard_id: $stateParams.medcard_id
            });
        });

        //$scope.efficiencymarks = EfficiencyMarks.queryBy({ medcard_id: $stateParams.medcard_id, isArray: true });

    }


}