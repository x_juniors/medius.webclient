﻿/**
 * @ngInject
 */
function NewOperationController($scope, $stateParams, $state, Operation) {
    $scope.loadNewOperation = function() {
        $scope.newOperation = {
            number: null,
            title: null,
            date: null,
            anesthesiaMethod: null,
            complications: null,
            surgeon: null,
            anesthetist: null
        }
    }

    $scope.loadNewOperation();

    $scope.saveNewOperation = function(data) {
        var newOperation = angular.copy(data);

        Operation.save({ medcard_id: $stateParams.medcard_id }, newOperation, function () {
            $scope.loadOperations();
            $state.go('app.medcard.treatment.operations');
        });
    }

    $scope.cencelNewOperation = function () {
        $state.go('app.medcard.treatment.operations');
    }
    
}